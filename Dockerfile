# Используем базовый образ Python
FROM python:3.10

# Установка переменной среды для запуска в режиме production (необязательно)
ENV DJANGO_ENV production

# Установка рабочей директории внутри контейнера
WORKDIR /app

# Копируем файлы зависимостей и устанавливаем их
COPY requirements.txt /app/
RUN pip install -r requirements.txt

# Копируем код проекта в контейнер
COPY . /app/

# Собираем статические файлы Django (если требуется)
# RUN python manage.py collectstatic --noinput

# Определение порта, на котором будет работать ваше приложение
EXPOSE 8000

